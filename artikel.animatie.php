<!DOCTYPE html>
<html lang="nl-NL">
	<head>
		<meta charset="utf-8">

		<!-- jongen met iPhone opent artikel over ruimtetoerisme met animatie -->
		<title>Dit kun jij zijn (over 14 jaar) - NRC Next</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-title" content="nrc.next">

		<link rel="stylesheet" href="//static.nrc.nl/fonts/guardian/headline/fonts.css">
		<link rel="stylesheet" href="//static.nrc.nl/fonts/guardian/web/fonts.css">
		
		<link rel="stylesheet" href="css/proto.css">
		<link rel="stylesheet" href="css/proto.post.css">
		
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" href="apple-touch-icon-ruimte.png">
	</head>

	<body class="artikel">
		<?php include('includes/menu.inc.php'); ?>

		<article class="video ruimte">
			<header>
				<figure>
					<img src="pulp/beeld/astronaut.gif" alt="Astronaut maakt een ommetje">
				</figure>

				<div class="kop">
					<h4>Ruimtetoerisme</h4>
					<h1>Dit kun jij zijn<br>
						(over 14 jaar)</h1>
				</div>
			</header>

			<?php include('includes/liefde.inc.php'); ?>
			
			<div class="tekst">
				<p class="intro">Voor het eerst besteedt NASA op grote schaal werk voor de bemande ruimtevaart uit.</p>

				<div class="byline">
					<div class="auteurs"> 
						<img src="optimum/auteurs/hoogstraten.jpg" alt="Diederik van Hoogstraten" class="avatar">
						Door 
						<ul>
							<li>Diederik van Hoogstraten</li>
						</ul>
					</div>
				
					<div class="datum">
						<time>16 oktober 2014</time>
					</div>
				</div>

				<p>De spaceshuttles staan geparkeerd in musea. Het budget voor ruimtevaartdienst NASA krimpt elk jaar. Nee, het ‘nabije heelal’ is geen prioriteit meer voor de Amerikaanse overheid, beaamt Scott Hubbard. En toch, zegt de toonaangevende ruimtevaartexpert, „vrijwel alle pogingen en energie op dit gebied lijken van de VS te komen”.</p>
				
				<h3>Trips naar ISS</h3>
				<p>Dat komt volgens Hubbard door de experimentele samenwerking tussen de publieke sector (NASA) en enkele bedrijven die voort willen bouwen op de ervaring die NASA sinds 1958 heeft opgebouwd.</p>
				
				
				<aside class="streamer">
					<p>Schatrijke ondernemers/ avonturiers gaan voorop in privatisering van de ruimte</p>
				</aside>
				
				
				<p>Vorige week werd bekend dat NASA een miljardencontract heeft gesloten met vliegtuigbouwer Boeing en ruimtevaart-onderneming SpaceX. De bedrijven gaan er voor zorgen dat Amerikaanse astronauten ook na 2017 naar het International Space Station (ISS) kunnen reizen. Met het meerjarige contract is een bedrag van 5,2 miljard euro gemoeid.</p>
				
			
				<h3>Russische ruimteschepen</h3>

				<p>Momenteel zijn de VS afhankelijk van Russische ruimteschepen voor trips naar het ISS. Wegens de kosten – 55 miljoen euro per retourvlucht – en de moeizame verhoudingen met Moskou willen de Amerikanen af van die samenwerking. Zodoende werden bedrijven uit de groeiende particuliere ruimtevaartsector uitgenodigd om zich te melden voor het contract om rakketten en astronautenmodulen te bouwen.</p>
				
				<p>Voor het eerst zal NASA op grote schaal werk voor de bemande ruimtevaart uitbesteden. „Een nieuw zakelijk economisch systeem”, noemt Hubbard dat waarderend. „De creativiteit van de Amerikaanse knowhow” in de particuliere sector ziet hij als een geheim wapen van de VS. Door afstand te nemen van de oude aanpak – waarin de overheid alles deed – en zich te richten op de innovatie van de private sector, hopen de Verenigde Staten de Chinese en Russische concurrentie voor te blijven.</p>
				
				
				<h3>Betrouwbare partners</h3>
				
				<p>Boeing en SpaceX zijn daartoe geschikte partners, oordeelt Hubbard, hoogleraar aan Stanford University en voorheen werkzaam bij NASA. NASA zal nauw betrokken blijven, zeker op het gebied van veiligheid, maar de twee ondernemingen hebben zich bewezen als betrouwbare partners.</p>
				
				<p>Boeing was ook betrokken bij de bouw van de spaceshuttles. Het Californische bedrijf van uitvinder en miljardair Elon Musk is een nieuwkomer. Space Exploration Technologies Corporation (SpaceX) vervoert momenteel goederen naar het ISS met ruimteschepen als de Falcon 9 en Dragon. Maar de ambitie van Musk gaat verder dan transport alleen.</p>
				
				<p>Musk beschouwt zichzelf als visionair. Zoals ook in zijn andere bedrijven – Tesla voor elektrische auto’s, SolarCity voor zonne-energie – wil hij niet alleen geld verdienen, maar de wereld verbeteren. „Dit bedrijf werd niet opgericht om T-shirts, eten en water naar de ruimte te brengen”, zei de SpaceX-manager Garrett Reisman kort geleden tijdens een conferentie.</p>
				
				<p>De nieuwe ondernemingen die voorop gaan in de privatisering van de ruimtevaart worden geleid door schatrijke ondernemers/avonturiers. De Brit Richard Branson is met Virgin Galactic, onderdeel van zijn Virgin Group, bezig om ruimtetoerisme mogelijk te maken. Jeff Bezos, de oprichter en topman van onlinewinkel Amazon, begon Blue Origin, eveneens om trips op grote hoogte algemeen beschikbaar en betaalbaar te maken. Blue Origin deed tevens een bod op het nieuwe contract met NASA, maar viel af. Ook de combinatie van Lockheed Martin en techbedrijf Sierra Nevada greep mis.</p>
				
				<p>Commercialisering en privatisering zijn altijd nodig voor innovatie en grote sprongen voorwaarts, zegt Hubbard. Hij vergelijkt de overgang van een ‘government only’-filosofie (100 procent overheid) naar het bedrijfsleven met de eerste commerciële luchtvaart van een eeuw geleden. Ook toen werden pionierbedrijven verwelkomd, om steeds meer taken van de eerste overheidsdiensten over te nemen en de technische vernieuwing in een stroomversnelling te brengen.</p>
				
				<p>Als Boeing en SpaceX efficiënt – en voor een lagere prijs – kunnen innoveren en bouwen, zoals wordt gehoopt, maakt dat de weg vrij voor NASA om verder te kijken. „Verken deep space” – dat is volgens Hubbard wat NASA moet doen. Een tocht naar Mars is het „ultieme doel”, zegt hij. Dat wordt mogelijk door oude en nieuwe bedrijven te betrekken bij de taken waar geen overheidsgeld meer voor is.</p>
				
				<p>„Ik zie geen commerciële mogelijkheden voor tochten naar de maan en Mars”, zegt Hubbard. „Maar het lagere werk? Uitbesteden.”</p>
				
				
				<p class="noot">Een versie van dit artikel verscheen op donderdag 9 oktober 2014 in nrc.next.<br>
					Op dit artikel rust auteursrecht van NRC Handelsblad BV, respectievelijk van de oorspronkelijke auteur.</p>
			</div>
		</article>

		<script src="js/jquery-2.1.1.min.js"></script>
		<script src="js/optimum.delen.js"></script>
	</body>
</html>
