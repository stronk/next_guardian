<!DOCTYPE html>
<html lang="nl-NL">
	<head>
		<meta charset="utf-8">

		<!-- meisje met iPad opent artikel over Uber met animatie -->
		<title>[ALT] Uber, dat is een taxi mét service - NRC Next</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-title" content="nrc.next">

		<link rel="stylesheet" href="//static.nrc.nl/fonts/guardian/headline/fonts.css">
		<link rel="stylesheet" href="//static.nrc.nl/fonts/guardian/web/fonts.css">
		
		<link rel="stylesheet" href="css/proto.css">
		<link rel="stylesheet" href="css/proto.post.css">
		
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" href="apple-touch-icon-taxi-alt.png">
	</head>

	<body class="artikel">
		<?php include('includes/menu.inc.php'); ?>

		<article class="foto taxiwereld">
			<header>
				<figure>
					<img src="pulp/beeld/onderweg.jpg" alt="Taxidriver">
				</figure>

				<div class="kop">
					<h4>Economie</h4>
					<h1>Uber, dat is een taxi mét service</h1>
				</div>
			</header>
			
			
			<?php include('includes/liefde.inc.php'); ?>
			
			
			<div class="tekst">
				<p class="intro">Taxi’s laten rijden zonder vergunning mag officieel niet, maar gebeurt steeds meer. In San Francisco zijn de grootste twee bedrijven, Uber en Lyft, in een felle concurrentiestrijd verwikkeld.</p>

				<div class="byline">
					<div class="auteurs"> 
						<!--<img src="pulp/auteurs/wout.jpg" alt="Mirik Milan en Ton Nabben" class="avatar">-->
						Door 
						<ul>
							<li>Mirik Milan</li>
							<li>Ton Nabben</li>
						</ul>
					</div>
				
					<div class="datum">
						<time>17 oktober 2014</time>
					</div>
				</div>

				<p>‘Natuurlijk vind ik dit werk leuk.” De 26- jarige Tarin Chiarakul kijkt bijna verontwaardigd bij de vraag. „Ik heb nog nooit zo makkelijk geld verdiend. Ik ontmoet nieuwe mensen, bedenk zelf wanneer ik wil werken en het enige wat ik nodig heb is mijn auto. Ik kan gewoon niet geloven dat dit zo makkelijk gaat.”</p>


				<h3>Kauwgom? Waterflesje?</h3>
				
				<p>Chiarakul is chauffeur in San Francisco voor ‘Lyft’, een taxistart-up. Net als het alternatieve taxibedrijf Uber laat Lyft particulieren met hun eigen auto mensen vervoeren. Zonder taxivergunning, zonder iets aan te trekken van wetgeving.</p>
						
				<p>Chauffeur Chiarakul zit er niet mee. Op een drukke dag verdient hij zo’n 300 dollar, 225 euro. Veel meer dan bij zijn vorige baan als ober. Trots wijst hij naar het zijportier. „Kauwgom? Waterflesje? Zal ik je telefoon opladen?” En dan, met een grote grijns: „Service he. Dit krijg je niet in een normale taxi.”</p>
					
				<p>De start-ups die voor taxi spelen – buiten taxicentrale en wetgeving om – groeien. Uber, begonnen in 2009, is verruit de grootste en bekendste. In Nederland lanceerde het bedrijf de Uber-taxi’s in 2012. Maar waar Nederland nog moet wennen aan een dienst als Uber, is die in San Francisco heel normaal.</p>
				
				<p>Sterker, de inwoners van de stad kunnen kiezen uit verschillende taxi-apps. Wil je luxe? Dan kies je voor een taxi van Uber. Uber heeft, doordat het ooit met chique zwarte auto’s begon, een wat formeler imago. De grote concurrent Lyft – nog niet in Nederland – heeft zich bewust tegen dat luxe imago afgezet en laat zijn chauffeurs rondrijden met een grote, felroze pluche snor op de bumper. Ook moedigt Lyft zijn gebruikers aan om naast de bestuurder te zitten – inplaats van de achterbank - en als groet en afscheid geeft de chauffeur een joviale ‘fist bump’.</p>


				<h3>Lyft en Uber zowat identiek</h3>

				<p>Maar verder zijn beide bedrijven zowat identiek. Ze hebben beide een snelle, intuïtieve app, staan allebei bekend om hun klantvriendelijkheid en zijn goedkoper dan een doorsnee taxi. Beide hebben hun hoofdkantoor in San Francisco. Uber zit in 170 steden in 44 landen wereldwijd. Lyft is kleiner, zit nu in 72 steden in de Verenigde Staten en zegt snel te groeien.</p>
				
				<p>En beide zijn ze verwikkeld in een heftige concurrentiestrijd om marktleider te worden, door zoveel mogelijk chauffeurs aan te trekken. Want wie de meeste chauffeurs heeft, zal ook de grootste naamsbekendheid en klantenkring opbouwen.</p>


				<div class="inzet">
					<h3>Hoe werken Uber en Lyft?</h3>
					
					<p>Klanten kunnen met de app op hun telefoon een taxi zoeken in de buurt. De app ziet zelf waar je precies bent, en laat op een plattegrond zien waar de taxi’s rijden. Na het maken van een reservering zie je de taxi realtime naar je toe komen, zodat je weet hoe lang je nog moet wachten. Betalen van de rit gaat via de creditcard die je koppelt aan de app. Naderhand kunnen klant en chauffeur elkaar een beoordeling geven.</p>

					<p>Vanaf deze maand laat Uber in Nederland ook particulieren met auto’s rijden voor Uberpop, de versie waarbij iedere particulier zonder taxivergunning klanten kan rondrijden. Legaal is het niet – je riskeert als chauffeur een boete van 4.200 euro. De pool met chauffeurs is nog klein, zo’n 25 auto’s.</p>
				</div>

				
				<p>Investeerders geloven in de bedrijven, die overigens notoir geheimzinnig doen over hun omzet en winst. Beleggers waarderen Uber op een bedrag tussen 13 tot 18 miljard dollar (9,8 à 13,6 miljard euro). Lyft – sinds 2012 – werd in april gewaardeerd op 700 miljoen dollar (530 miljoen euro).</p>

				<p>De bedrijven zijn een succes, niet alleen omdat ze hard groeien – Uber is al paar keer onofficieel uitgeroepen tot snelst groeiende bedrijf. Ook de belofte die ze voor de toekomst bieden, speelt mee bij het enthousiasme bij investeerders: wat nú nog een app is die de taxibranche op z’n kop zet, kan uitgroeien tot een platform dat alle logistieke zaken regelt. Zo begon Uber vorige week een bezorgdienst in Washington, en levert het onder meer luiers, tandpasta en medicijnen aan huis.</p>
				

				<h3>Teams gewapend met telefoontjes</h3>
				
				<p>De strijd om marktleider te worden is al bezig sinds Lyft begon, maar wordt steeds publieker uitgevochten. De bestuursvoorzitters vliegen elkaar op Twitter in de haren. Beide bedrijven bieden chauffeurs grote bedragen (500 dollar) en gratis lunches. Lyft heeft Uber meermalen beschuldigd van het maken van nepaccounts: medewerkers van Uber zouden expres een taxi van Lyft aanvragen om die vervolgens te af te zeggen. Het zou om meer dan vijfduizend ritjes gaan.</p>

				<p>Deze week onthulde techblog The Verge, op basis van gelekte e-mails en gesprekken, dat Uber-chauffeurs getraind worden om Lyft-chauffeurs over te halen om over te stappen. Toen Lyft vorige maand met veel bombarie zijn diensten ook in New York aanbood, stuurde Uber speciale teams erop uit onder de naam ‘Operation SLOG’. Gewapend met extra mobieltjes om valse accounts aan te maken en op die manier zoveel mogelijk taxi’s van Lyft te bestellen. In instructiemailtjes naar deze teams stond precies hoe ze een Lyft-chauffeur (vriendelijk) moeten overtuigen toch voor Uber te werken en werd de hashtag #shavethestache gebruikt, wat verwijst naar de roze snor van Lyft.</p>

				<p>Alhoewel Uber als reactie op de onthulling op hun website ontkent „doelbewust ritjes aan te vragen en dan te cancelen”, geven ze wel toe dat ze speciale recruitmentteams hebben om Lyftchauffeurs te ‘bekeren’. Maar andersom worden Lyftchauffeurs óók aangemoedigd om Uberchauffeurs over te halen voor Lyft te beginnen, volgens het bedrijf.</p>


				<h3>Freelancers rijden voor beide</h3>

				<p>De strijd is zo heftig omdat beide bedrijven gebruik maken van freelancechauffeurs, die elke dag zelf mogen bepalen voor wie ze werken. Zo heeft chauffeur Junior Tagata (44) op zijn dashboard de grote roze, pluche snor van een halve meter lang liggen, het kenmerk van Lyft. Eigenlijk hoort die op de voorkant van de bumper, maar Tagata werkt zowel voor Uber als Lyft.</p>

				<p>Bestelt iemand via Uber een taxi, moffelt dan moffelt Tagata snel de snor in het dashboardkastje. Bij een oproep van Lyft legt hij hem weer in het zicht. Hij heeft twee mobieltjes nodig om beide apps tegelijkertijd in de gaten te houden. „Ik rij voor het bedrijf dat me als eerste oproept.”</p>

				<p>Opbrengst per dag: rond de 250 dollar, daar rijdt hij zo’n 28 mensen voor rond. Hij doet dit sinds maart en onderhoudt zijn gezin met vier kinderen ervan. „Ik was hiervoor muziekleider in een kerk.” Betaalt dit beter? „Hell yeah.”</p>


				<h3>Gezellig</h3>

				<p>Tagata heeft een lichte voorkeur voor Lyft. „Het is gewoon gezelliger als mensen naast je komen zitten.” Dit werk is precies wat hij nodig heeft. „Je kletst de hele dag, je leert mensen kennen, ik rij lekker rond in mijn mooie stad.”</p>

				<p>Als hij zijn kinderen van school moet halen, stopt hij even, daarna zet hij zijn mobiel weer aan en is hij weer beschikbaar voor Lyft of Uber. Hij sluit niet uit dat uiteindelijk één van de twee bedrijven zal overblijven. „Ik rij gewoon voor diegene die me de beste voorwaarden geeft en aan wie ik het minste percentage van de winst hoef af te dragen.”</p>

				<p>En dat is nu de beste troef voor de bedrijven: de chauffeurs paaien met geld. Van elke rit die een klant afrekent maakt, moeten chauffeurs 20 procent afdragen aan Uber of Lyft. Van geld dat ze overhouden, moeten chauffeurs nog belasting en de benzine betalen.</p>

				<p>Lyft besloot deze maand dat naarmate chauffeurs meer uren rijden, ze minder hoeven af te dragen van de 20 procent die naar het bedrijf gaat. Wie meer dan 50 uur in de week rijdt, hoeft tijdelijk helemaal niks af te dragen. Uber deed eerder al eens hetzelfde, waardoor het ook tijdelijk geen winst maakte op ritten.</p>

				<p>Dat de bedrijven elkaar in de nek hijgen, is ook te zien aan ander kopieergedrag. Begin deze maand lanceerde Lyft de dienst Lyft Line. Dan deel je de rit ook met andere passagiers die toch dezelfde kant op moeten, en betaal je navenant minder. Een avond voor de start kondigde Uber precies zo’n zelfde dienst aan: UberPool.</p>


				<h3>Aanzwellende kritiek</h3>

				<p>Ondertussen hebben de bedrijven ook te maken met aanzwellende kritiek. Uber meer dan Lyft, omdat het nou eenmaal groter en bekender is. Kritiek komt niet alleen van overheden en taxibedrijven. Ook van de chauffeurs zelf. Over de verzekering is nog veel onduidelijkheid. „Ik weet nog steeds niet hoe het nou zit als ik een ongeluk veroorzaak”, vertelt Lyft-chauffeur Ariana. „Ze zeggen dat ik verzekerd ben, maar je weet nooit zeker hoe ze daar onderuit kunnen komen. Ik probeer maar gewoon geen ongeluk te maken.”</p>

				<p>Ook de baanperspectieven voor de chauffeurs zijn niet rooskleurig. „Ik doe het nog een half jaar. Of vooruit, een jaar.” zegt Ariana. „Even sparen, en daarna wil ik een echte baan zoeken. Dit is te makkelijk, als ik dit blijf doen ben ik over een aantal jaar nog steeds taxichauffeur.”</p>

				<p>En dan, half serieus, half voor de grap: „Je weet niet hoe het er hier over vijf jaar uitziet. Straks rijden er alleen nog maar automatische auto’s rond, dan zijn chauffeurs sowieso overbodig.”</p>
				
				
				<p class="noot">Een versie van dit artikel verscheen op donderdag 9 oktober 2014 in nrc.next.<br>
					Op dit artikel rust auteursrecht van NRC Handelsblad BV, respectievelijk van de oorspronkelijke auteur.</p>
			</div>
		</article>

		<script src="js/jquery-2.1.1.min.js"></script>
		<script src="js/optimum.delen.js"></script>
	</body>
</html>
