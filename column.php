<!DOCTYPE html>
<html lang="nl-NL">
	<head>
		<meta charset="utf-8">

		<!-- meisje met iPad opent artikel over Uber met animatie -->
		<title>[ALT] Uber, dat is een taxi mét service - NRC Next</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-title" content="nrc.next">

		<link rel="stylesheet" href="//static.nrc.nl/fonts/guardian/headline/fonts.css">
		<link rel="stylesheet" href="//static.nrc.nl/fonts/guardian/web/fonts.css">
		
		<link rel="stylesheet" href="css/proto.css">
		<link rel="stylesheet" href="css/proto.post.css">
		
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" href="apple-touch-icon-taxi-alt.png">
	</head>

	<body class="artikel">
		<?php include('includes/menu.inc.php'); ?>

		<article class="column marcel">
			<header>
				<div class="kop">
					<?php include('includes/liefde.inc.php'); ?>

					<img src="pulp/auteurs/mvanroosmalen.jpg" alt="Marcel van Roosmalen" class="avatar">

					<h1><span class="columnist">Marcel</span> Een rode japon tegen huiselijk geweld</h1>
				</div>
			</header>
			
			
			<div class="tekst">
				<p>Een druilerige ochtend in Amsterdam. Het land had nog geen mening over de breuk tussen roddelkoning Albert en zijn Onno. Peter R. de Vries was nog niet begonnen aan zijn ronde langs talkshows om kond te doen van de ernst en omvang van de bedreigingen die hem ten deel waren gevallen sinds hij een mening had over zwarte piet.</p>
				
				<p>Het aangekondigde nieuws in de vorm van Koningin Máxima was onderweg naar hier, naar het Tropenmuseum, naar de viering van veertig jaar ‘Blijf van m’n lijf’. De eerste plukjes belangstellenden stonden al over de dranghekken geleund, mobiele telefoons in de hand. Een wagen van de gemeentereiniging spoot het wegdek schoon, opdat de Koningin in ieder geval niet zou denken dat Amsterdam een smerige stad was.</p>

				<p>Het was een flitsbezoek, het hoogtepunt zou zich binnen afspelen waar ze met een koninklijke vinger een nieuwe app voor slachtoffers van huiselijk geweld zou lanceren. De hulpverleenster uit wiens brein de app ‘met chat- en dagboekfunctie’ was ontsproten, was op het schoongespoten wegdek al een paar keer geïnterviewd. Ze omschreef haar vinding als ‘nuttig’.</p>
				
				<p>Een app voor als je thuis in elkaar geslagen bent, het leek me dat de hulpverlening nu wel voldoende was gedigitaliseerd. Maar ik was de enige die zo dacht. De mensen om me heen focusten op andere zaken.</p>
				
				<p>„Aaah”, kreunde de vrouw naast me, toen de koningin uit een zwarte auto stapte. „Een rode japon van modehuis Natan en een bijpassende flaphoed van Fabienne Delvigne.”</p>

				
				<aside class="streamer">
					<p>Wat Máxima ons wilde vertellen met deze combinatie <b>bleef gissen</b></p>
				</aside>
						
				
				<p>Het was een taal uit een andere wereld, toen Máxima uit beeld was verdwenen zei ze dat ze die combinatie al eerder had gezien, bij een bezoek aan Haarlem begin dit jaar. „Kleding is een statement, het is zo’n beetje de enige manier waarop ze zich kan uiten”, werd me verteld. Maar wat de Koningin ons wilde vertellen met deze rode combinatie bleef gissen.</p>
				
				<p>„Rood is een vurige kleur, ik denk aan een statement.”</p>
		
				<p>Even later, de app voor slachtoffers van huiselijk geweld was gelanceerd, kwam ze weer naar buiten. Dit keer vergezeld van een hofdame die een paraplu boven de flaphoed hield. Twee keer zwaaien, toen reed ze weg.</p>
				
				<p>Naast me waren ze benieuwd wat Máxima die middag in Paleis Noordeinde, de Spaanse koning en koningin kwamen op bezoek, aan zou hebben.</p>
				
				<p>Misschien wel hetzelfde, opperde ik.</p>
				
				<p>„Uitgesloten”, werd er gezegd, „maar de gedachte is sympathiek.”</p>
				
				<p>Ik ging maar weer eens, als we op die toer gingen, was alles sympathiek.</p>


				<p class="auteur noot"><strong>Marcel van Roosmalen</strong> (1968) is auteur van onder meer Op Pad met Pim (2002), een bundeling van reportages waarin hij de politieke campagne van Pim Fortuyn van nabij beschreef, Op Campagne met Oranje (2004) en verspreid over enkele jaren, een drieluik over drie verschillende seizoenen met Vitesse. </p>
				
				<p class="noot">Een versie van deze column verscheen op donderdag 9 oktober 2014 in nrc.next.<br>
					Op dit artikel rust auteursrecht van NRC Handelsblad BV, respectievelijk van de oorspronkelijke auteur.</p>

			</div>
		</article>

		<script src="js/jquery-2.1.1.min.js"></script>
		<script src="js/optimum.delen.js"></script>
	</body>
</html>
