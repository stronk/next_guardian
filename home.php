<!DOCTYPE html>
<html lang="nl-NL">
	<head>
		<meta charset="utf-8">

		<title>NRC Next Optimum</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-title" content="nrc.ruimte">

		<link rel="stylesheet" href="//static.nrc.nl/fonts/guardian/headline/fonts.css">
		<link rel="stylesheet" href="//static.nrc.nl/fonts/guardian/web/fonts.css">
		
		<link rel="stylesheet" href="css/proto.css">

		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
	</head>

	<body class="home">
		<?php include('includes/menu.inc.php'); ?>

		<main>
			<div class="post foto groot ruimte">
				<a href="/artikel.php" class="blijf-in-app">
					<figure>
						<img src="pulp/beeld/astronaut.jpg" alt="ruimtetoerist maakt een ommetje">
					</figure>

					<header>
						<h4>Ruimtetoerisme</h4>
						<h2>'Als de top minder verdient, zal de hele organisatie minder krijgen'</h2>
					</header>
				</a>
			</div>


			<div class="post column marcel">
				<a href="column.php" class="blijf-in-app">
					<header>
						<h2><span class="columnist">Marcel</span> Een rode japon tegen huiselijk geweld</h2>
					</header>
				</a>
			</div>


			<div class="post foto obama">
				<a href="artikel.fotoloos.php" class="blijf-in-app">
					<figure>
						<img src="pulp/beeld/obama.jpg" alt="Obama te water">
					</figure>
	
					<header>
						<h4>Amerika</h4>
						<h2>Obama. Oorlogspresident?</h2>
					</header>
				</a>
			</div>


			<div class="post foto zwanger">
				<a href="artikel.animatie.php" class="blijf-in-app">
					<figure>
						<img src="pulp/beeld/zwanger.jpg" alt="zwangere recordpoging">
					</figure>
	
					<header>
						<h4>Gezondheid</h4>
						<h2>Iedereen zwanger? Ja!</h2>
					</header>
				</a>
			</div>


			<div class="post video wad">
				<a href="/column.php" class="blijf-in-app">
					<figure>
						<img src="pulp/beeld/zee.jpg" alt="de zee kabbelt">
					</figure>
	
					<header>
						<h4>De Waddenzee</h4>
						<h2>Waar zijn de zeehondjes?</h2>
					</header>
				</a>
			</div>


			<div class="post foto taxiwereld">
				<figure>
					<img src="pulp/beeld/onderweg.jpg" alt="een ritje door de stad">
				</figure>

				<header>
					<h4>Economie</h4>
					<h2>Uber, dat is een taxi<br>
						mét service</h2>
				</header>
			</div>


			<div class="post foto koerden">
				<figure>
					<img src="pulp/beeld/koerden.jpg" alt="Meevechten met Koerden">
				</figure>

				<header>
					<h4>Midden-Oosten</h4>
					<h2>Voor vrede is nu eenmaal oorlog nodig</h2>
				</header>
			</div>


			<div class="post foto cameron">
				<figure>
					<img src="pulp/beeld/cameron.jpg" alt="Cameron">
				</figure>

				<header>
					<h4>Buitenland</h4>
					<h2>Onverenigd Koninkrijk</h2>
				</header>
			</div>


			<div class="post foto elf">
				<figure>
					<img src="pulp/beeld/elf.jpg" alt="Gekostumeerde elf">
				</figure>

				<header>
					<h4>Fantasyfestivals</h4>
					<h2>Neem je zwaard mee</h2>
				</header>
			</div>


			<div class="post foto corbijn">
				<figure>
					<img src="pulp/beeld/corbijn.jpg" alt="Anton Corbijn">
				</figure>

				<header>
					<h4>Film</h4>
					<h2>Meet Anton Corbijn, Dutch master</h2>
				</header>
			</div>
		</main>
		
		<script src="js/jquery-2.1.1.min.js"></script>
		<script src="js/optimum.delen.js"></script>
	</body>
</html>
