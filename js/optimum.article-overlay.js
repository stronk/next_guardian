$('a.fade-in').each(function() {
	var $link = $(this);
	var $body = $('body');
	var $iframe = $('<iframe/>')
		.attr('src', this.href)
		.addClass('article-overlay')
		.appendTo( $('body') );

	$link.on('click', function(event) {
		event.preventDefault();

		$body.addClass('pre-fade-in-active');

		setTimeout(function() {
			$body.addClass('fade-in-active');
		}, 0);

		setTimeout(function() {
			$body.addClass('post-fade-in');
			document.body.scrollTop = 0;
		}, 600);	// iets langer dan de ~.5s duration zoals aangegeven in css

	});
});


// Open deze links binnen de web-app als dat kan en nodig is
// http://blog.cloudfour.com/seven-things-i-learned-while-making-an-ipad-web-app-2/
$(document).ready(function() {
  $(function() {
      $.stayInWebApp('a.blijf-in-app');
  });
});


