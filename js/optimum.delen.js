// Helpt bij het maken van de modal die je ziet na "delen"
var pop = $('#popup'),
	toggle = $('.toggle');

toggle.click(function() {
	pop.fadeToggle(600, function() {});
});


// Open deze links binnen de web-app als dat kan en nodig is
// http://blog.cloudfour.com/seven-things-i-learned-while-making-an-ipad-web-app-2/
$(document).ready(function() {
  $(function() {
      $.stayInWebApp('.blijf-in-app');
  });
});


