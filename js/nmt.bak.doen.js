nmt.bak = nmt.bak || {};

// SEO: force consistent hostname www.nrc.nl, never m.nrc.nl or www.m.nrc.nl
if (location.host.match(/^(m\.nrc\.nl|www\.m\.nrc\.nl)$/)) {
	location.replace('http://www.nrc.nl' + location.pathname + location.search + location.hash);
}

// strip ?ustatus=authenticated param van url om te voorkomen dat mensen het in die vorm sharen,
// wat de social counts niet ten goede komt
(function() {
	if (history.replaceState && location.search.indexOf('?ustatus=authenticated') > -1) {
		var href = location.href;
		href = href.replace(/\?ustatus\=authenticated/g, '');
		href = href.replace('/&', '/?');
		history.replaceState({}, null, href);
	}
})();


// activeer multi-image-slider met indicator-bolletjes
nmt.bak.swipeIt = function() {
    if (!window.Swipe) { return; }

    var $slider = $('#slider');

    var fnSwipeCallback = function(pos, elem) {
        var $bullets = $('#indicator li');
        var realpos = pos;

        // N.B. als gevolg van Swipe continuous scroll worden de slide-elementen gedubbeld;
        // dus verminder pos met echte maximum om toch juiste indicator te kunnen highlighten
        if (pos >= $bullets.length) {
            pos -= $bullets.length;
        }

        $bullets
            .removeClass('actief')
            .eq(pos).addClass('actief');

        nmt.log('swipe callback: highlight indicator', pos);
     };

    var swipe = Swipe( $slider[0], {
        callback: fnSwipeCallback,
        continuous: true
    } );

    nmt.log('init swipe');

    $slider.on('click', '.go.next', function(e) {
        e.preventDefault();
        swipe.next();
    });
    $slider.on('click', '.go.prev', function(e) {
        e.preventDefault();
        swipe.prev();
    });
};	// nmt.bak.swipeIt

nmt.bottom.push(function() {
	nmt.bak.swipeIt();
});


nmt.bak.adjustSluierLayout = function() {
	if ($('body').hasClass('sluier-is-visible')) {
		var $articleBody = $('.body'),	// niet te verwarren met <body>
			h = $('.sluier').outerHeight();
		$articleBody.outerHeight( h );
		nmt.log('nmt.bak.adjustSluierLayout: setting .body outerHeight to', h);
	}
};

nmt.bottom.push(function() {
    nmt.bak.adjustSluierLayout();

	// N.B. de boel opnieuw verschuiven bij switchen tussen portrait/landscape
    $(window).on(
    	nmt.is.touch ? 'orientationchange' : 'resize',
    	function() { nmt.bak.adjustSluierLayout(); }
    );
});


// load social counts
nmt.bottom.push(function() {
	if ($('.twitter .count').length) {
		$.getJSON("http://cdn.api.twitter.com/1/urls/count.json?callback=?&url=" + nmt.bak.ndata.article_url, function(data) {
			$(".twitter .count").html(data.count);
		});
	}

	if ($('.facebook .count').length) {
		$.getJSON("https://api.facebook.com/method/links.getStats?format=json&urls="+nmt.bak.ndata.article_url, function(data) {
			$(".facebook .count").html(data[0].total_count);
		});
	}

	if ($('.google .count').length) {
		$.getJSON("/static/g_plus.php?url="+nmt.bak.ndata.article_url,
			function(data) {
				$(".google .count").html(data[0].result.metadata.globalCounts.count);
			});
	}
});


nmt.is.touch && nmt.bottom.push(function() {
    $('.swipe a.go').remove();
});

nmt.bottom.push(function() {
	// bijschrift aan- en uittikken
	var sliderTapCount=0;
	$('#slider .foto figcaption').addClass('hide');
	$('#slider .foto').prepend('<i>');
	$('#slider .foto i').addClass('unhide');

	$('#slider .foto img').click( function(){
		sliderTapCount++;
		if (sliderTapCount % 2 !== 0) {
			$('#slider .foto i').toggleClass('unhide hide');
			$('#slider .foto figcaption').toggleClass('hide unhide');
		} else {
			$('#slider .foto i').toggleClass('hide unhide');
			$('#slider .foto figcaption').toggleClass('unhide hide');
		}
	});

	$('figcaption').filter(function() {
		return $.trim($(this).text()) === '';
		}).remove();

	$(".bericht .verberg").click(function(){
		$(this).parents(".bericht").animate({ opacity: 'hide' }, "slow");
	});
});


nmt.bottom.push( function() {
	var slidemenu_ignore_click = false;
	$("#slidemenu").on('touchstart click', function (e) {
		e.preventDefault();
		if (!slidemenu_ignore_click) {
			slidemenu_ignore_click = true;
			setTimeout(function(){ slidemenu_ignore_click = false; }, 100);
			$('.slidemenu').toggleClass('slidemenu-reveal');
		}
	});
});

nmt.bottom.push( function() {
	// ping even uitstellen tot window.load, omdat nmt.common.js nu waarschijnlijk nog niet geladen is;
	// (todo: beter nog zou zijn een queue-structuur hiervoor verzinnen, zoals nmt.bottom.push)
	//
	$(window).on('load', function() {
		nmt.trackers.klik.ping({
			from: document.location.href
		});
	});
});

nmt.bottom.push( function() {
	var allSections = $('.overzicht > ul').hide();
	if(window.location.hash) {
		console.log(window.location.hash);
		$('.overzicht > ul' + window.location.hash).slideDown();
	} else {
		$('.overzicht > ul').first().slideDown();
	}
	$('.overzicht > h1.section').click(function() {
		allSections.slideUp();
		$(this).next().slideDown();
		//return false;
	});
});


// grapje voor slimmerikken die zo bijdehant zijn om urls als /handelsblad/van/morgen of /next/van/overmorgen te proberen
location.href.match(/\/van\/(morgen|overmorgen)/i) && nmt.bottom.push(function() {
	var $img = $('<img/>');
	$img.css({width: '100%'});
	$img[0].src = '/static/img/timetravel/' + Math.floor(Math.random()*7) + '.gif';
	$('p:contains("weergegeven")').after($img);
});

